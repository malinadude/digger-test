import Link from 'next/link';
import styles from '../styles/Home.module.css';
import Header from "../components/header";
import Footer from "../components/footer";

function NotFound () {
    return (
        <>
            <Header />
            <div className={`${["notFound"]} ${["container"]}`}>
                <h1>404</h1>
                <h2>Страница удалена или не существовала вовсе!</h2>
                <p>Вы можете вернуться на <Link href='/'><a>главную</a></Link> или сразу перейти на одну из популярных новостей:</p>
            </div>
            <Footer />
        </>
    )
}
 
export default NotFound;