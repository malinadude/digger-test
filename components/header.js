import styles from '../styles/header.module.css'
import Soclinks from '../components/soclinks'
import Link from 'next/link'

function Header() {
  return (
    <header className={`${styles.header} ${["container"]}`}>
      <Link href='/'><a className={styles.header_logo}>
        <img src="/desktop-logo.png" width="240" height="84"/>
{/*         <svg svg="true" width="250" height="94">
            <use href="/digger-sprites.svg#image0_0_892"></use>
        </svg> */}
      </a></Link>
        <Soclinks color="#000"/>
        <p>войти</p>
      <div className={styles.navpanel}>
          <Link href="#1"><a>новости</a></Link>
          <Link href="#2"><a>гаджеты</a></Link>
          <Link href="#3"><a>технологии</a></Link>
          <Link href="#4"><a>люди</a></Link>
          <Link href="#5"><a>развлечения</a></Link>
          <Link href="#6"><a>#apple</a></Link>
        {/* <input type="search" /> */}
        <svg svg="true" width="24px" height="24px">
            <use href="/digger-sprites.svg#icon-search_icon"></use>
        </svg>
      </div>
    </header>
  )
}

export default Header