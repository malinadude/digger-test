import styles from '../styles/Home.module.css'
import Soclinks from '../components/soclinks'

function Footer() {
    return (
        <footer className={styles.footer}>
            <div className={styles.footer_links}>
                <div className={styles.footer_nav}>
                    <a href="">О MACDIGGER.RU</a>
                    <a href="">Рекламодателям</a>
                    <a href="">Правила</a>
                    <a href="">Политика</a>
                    <a href="">конфиденциальности</a>
                </div>
                <Soclinks color="#FFF"/>
            </div>
            <div className={styles.footer_text}>
                <p>© 2009 – 2019 MacDigger.</p>
                <p>Использование материалов допускается только при наличии активной ссылки на MacDigger.ru</p>
                <p>info@macdigger.ru</p>
            </div>
        </footer>
    )
}
 
export default Footer