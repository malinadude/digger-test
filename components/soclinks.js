import styles from '../styles/soclinks.module.css';

function Soclinks(props) {

    return (
        <div style={{'color':props.color}} className={styles.container}>
            <a href="https://www.facebook.com/diggerrru/">
                <svg svg="true" width="8px" height="16px" fill="currentColor">
                    <use href={"/digger-sprites.svg#icon-social-fb"}></use>
                </svg>
            </a>
            <a href="https://tlgg.ru/macdiggerr">
                <svg svg="true" width="19px" height="16px" fill="currentColor">
                    <use href="/digger-sprites.svg#icon-social-tg"></use>
                </svg>
            </a>
            <a href="https://vk.com/diggerru">
                <svg svg="true" width="18px" height="11px" fill="currentColor">
                    <use href="/digger-sprites.svg#icon-social-vk"></use>
                </svg>
            </a>
            <a href="https://twitter.com/ru_digger">
                <svg svg="true" width="15px" height="13px" fill="currentColor">
                    <use href="/digger-sprites.svg#icon-social-tw"></use>
                </svg>
            </a>
            <a href="https://zen.yandex.ru/diggerru">
                <svg svg="true" width="17px" height="17px" fill="currentColor">
                    <use href="/digger-sprites.svg#icon-social-yandex_zen"></use>
                </svg>
            </a>
            <a href="https://www.instagram.com/diggerru/">
                <svg svg="true" width="17px" height="17px" fill="currentColor">
                    <use href="/digger-sprites.svg#icon-social-inst"></use>
                </svg>
            </a>
        </div>
        )
}

export default Soclinks;